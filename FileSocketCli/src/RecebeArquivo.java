import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public class RecebeArquivo implements Runnable {

	String nomeArq;
	String endServer;
	int porta;
	Cliente cliente;
	Recebedor recebedor;
	
	public RecebeArquivo(String nomeArq, String endServer, int porta, Cliente cliente, Recebedor recebedor) {
		this.nomeArq = nomeArq;
		this.endServer = endServer;
		this.porta = porta;
		this.cliente = cliente;
		this.recebedor = recebedor;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			recebeArq();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

   private void recebeArq() throws IOException {  
	   Socket socket = new Socket(this.endServer, this.porta);
	   FileOutputStream fos = new FileOutputStream(this.nomeArq);
	   
	   BufferedOutputStream out = new BufferedOutputStream(fos);
	   
	   long startTime = System.nanoTime();
	   
	   byte[] buffer = new byte[1024];
	   int count;
	   InputStream in = socket.getInputStream();
	   
	   while((count=in.read(buffer)) >0){
		   out.write(buffer, 0, count);
	   }
	   
	   long endTime = System.nanoTime();
	   long duration = (endTime - startTime);
	    
	   int part = Integer.parseInt(this.nomeArq.substring(this.nomeArq.length() - 1));
	   
	   this.cliente.marcaTempo(duration/1000, part);
	   
	  
	   
	   fos.close();
	   socket.close();
	   
	   this.recebedor.partsOK += 1;
	   this.recebedor.uneArq();
   }
}
