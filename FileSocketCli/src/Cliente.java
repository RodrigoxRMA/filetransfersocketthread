import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cliente {
	   
		public static void main(String[] args) 
	      throws UnknownHostException, IOException {
		   
			 System.out.println("Entre com o endereco do servidor e a porta desejada. \n" +
			   		"Ex.: 127.0.0.1:12345");
			 Scanner teclado = new Scanner(System.in);  
			 
			 String[] endereco = teclado.nextLine().split(":");
			 if(endereco.length == 2)
				 // dispara cliente
				 new Cliente(endereco[0], Integer.parseInt(endereco[1])).executa();
			 else
				 System.out.println("Informacoes incorretas, tente novamente.");
	   }
	   
	   private String host;
	   private int porta;
	   private String file;
	   
	   public List<Long> Tempos;
	   
	   public long maiorTempo;
	   
	   public Cliente (String host, int porta) {
	     this.host = host;
	     this.porta = porta;
	     this.file = "";
	     this.Tempos = new ArrayList<Long>();
	     this.maiorTempo = 0;
	   }
	   
	   public void executa() throws UnknownHostException, IOException {
	     
		   
		 Socket cliente = new Socket(this.host, this.porta);
	     System.out.println("O cliente se conectou ao servidor!");
	 
	     // thread para receber mensagens do servidor
	     Recebedor r = new Recebedor(cliente.getInputStream(),this);
	     new Thread(r).start();
	     
	     // lê msgs do teclado e manda pro servidor
	     Scanner teclado = new Scanner(System.in);
	     PrintStream saida =  new PrintStream(cliente.getOutputStream());
	     
	     System.out.println("Entre com o nome do arquivo e a " +
	      		"quantidade \nque deseja a divisao dele no formato:" +
	      		"nomedoarquivo.extensao;partes");
	     
	     while (teclado.hasNextLine()) {
	    	 String msg = null;
		     
	    	 msg = teclado.nextLine();
     
	    	 saida.println(msg);
	     }
	     
	    saida.close();
	    teclado.close();
	    cliente.close();    
	   }
	   
	   public void marcaTempo(long tempo, int thread) {
		   this.Tempos.add(new Long(thread));
		   this.Tempos.add(tempo);
		   
		   if (tempo > this.maiorTempo)
			   this.maiorTempo = tempo;
		   
		   System.out.println("Thread " + thread + " finalizada em " + tempo + ".\n Maior tempo:  " + this.maiorTempo);
		   
		   
		   
	   }
	   
	   
}