import java.io.File;
import java.io.InputStream;
import java.util.Scanner;


 public class Recebedor implements Runnable {
 
   private InputStream servidor;
   private Cliente cliente;
   private String nomeArq;
   private int parts;
   public int partsOK;
 
   public Recebedor(InputStream servidor, Cliente cliente) {
     this.servidor = servidor;
     this.cliente = cliente;
     this.nomeArq = "";
     this.parts = 0;
     this.partsOK = 0;
   }
 
   public void run() {
	     // recebe msgs do servidor e imprime na tela
	     Scanner s = new Scanner(this.servidor);
	     
	     String msg = null;
	     
	     while (s.hasNextLine()){
	    	 msg = s.nextLine();
	    	 
		     if(msg.equals("###")){
		       
		       String[] args = s.nextLine().split(":");
		       
		       this.parts = Integer.parseInt(args[2]);
		       this.nomeArq = args[0];
		       
		       if (s.nextLine().equals("###")){
			       if (parts > 1) {
				       for(int i=0;i < this.parts;i++){
				    	   //RecebeArquivo String nomeArq, String endServer, int porta
				    	   RecebeArquivo ra = new RecebeArquivo(this.nomeArq + ".part." + i, args[3],Integer.parseInt(args[1]) + i + 1,
				    			   this.cliente,this);
				    	   
				    	   new Thread(ra).start();
				    	   System.out.println("Recebendo parte " + i);
				       }
			       }
			       else{
			    	   RecebeArquivo ra = new RecebeArquivo(this.nomeArq, args[3],Integer.parseInt(args[1]) + 1, this.cliente, this);
			    	   
			    	   new Thread(ra).start();
			    	   System.out.println("Recebendo arquivo.");
			       }
		       }
		       
		     }
		     else{
		    	 System.out.println(msg);
		     }
		     
		  
	     }
	     
	     s.close();
	    
     
   }
   
   public void uneArq() {
	   
	   if (this.partsOK >= (this.parts)){
		   
	     File file = new File(this.nomeArq + ".part.0");
		 FileSplitter splitter = new FileSplitter(file);
		 System.out.println("Juncao ok: " + splitter.unsplit());
		   
	   }
   }
   
   
 }
 