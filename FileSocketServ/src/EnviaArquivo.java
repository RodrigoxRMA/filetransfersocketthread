import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class EnviaArquivo implements Runnable {

	   private String nomeArq;
	   private int porta;
	   private int part;

	 
	   public EnviaArquivo(String nomeArq , int porta, int part) {
	     this.nomeArq = nomeArq;
	     this.porta = porta;
	     this.part = part;
	    

	   }
	
	
	@Override
	public void run() {
		try {
			sendFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}  
	
	private void sendFile() throws IOException{
		ServerSocket server_socket = new ServerSocket(this.porta);
		File myFile = new File(this.nomeArq);
		Socket socket = server_socket.accept();
		int count;
		byte[] buffer = new byte[1024];

		OutputStream out = socket.getOutputStream();
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(myFile));
			
		
		
		while ((count = in.read(buffer)) >= 0) {
		     out.write(buffer, 0, count);
		     out.flush();
		}
		
		
		myFile.delete();
		
		socket.close();
		
		System.out.println("Parte " + this.part + " enviada.");
		
		
	}
}
