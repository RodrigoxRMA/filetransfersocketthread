import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
 
public class Servidor {
	 
	   public static void main(String[] args) throws IOException {
		 System.out.println("Entre com o endereco da porta desejada para servir. \n" +
			   		"Ex.: 12345");
		 Scanner teclado = new Scanner(System.in);  
		 
		 String endereco = teclado.nextLine();
		 // inicia o servidor
		 new Servidor(12345).executa();
			 

	   }
	   
	   private int porta;
	   private List<PrintStream> clientes;
	   private int numCli;
	   public List<String> addrClientes;
	   

	   
	   public Servidor (int porta) {
	     this.porta = porta;
	     this.clientes = new ArrayList<PrintStream>();
	     this.numCli = 0;
	     this.addrClientes = new ArrayList<String>();
	     
	   }
	   
	   public void executa () throws IOException {
	     ServerSocket servidor = new ServerSocket(this.porta);
	     System.out.println("Porta 12345 aberta!");
	     
	     while (true) {
	       // aceita um cliente
	       Socket cliente = servidor.accept();
	       
	       this.addrClientes.add(cliente.getInetAddress().getHostAddress());
	       
	       System.out.println("Nova conexão com o cliente " +   
	         cliente.getInetAddress().getHostAddress()
	       );
	       
	       // adiciona saida do cliente à lista
	       PrintStream ps = new PrintStream(cliente.getOutputStream());
	       this.clientes.add(ps);
	       
	       // cria tratador de cliente numa nova thread
	       TrataCliente tc = new TrataCliente(cliente.getInputStream(), this,numCli, 
	    		   cliente.getInetAddress().getHostAddress(), this.porta );
	       new Thread(tc).start();
	     }
	 
	   }
	 
	   public void enviaMensagem(String msg,int Cli) {
		 
	     this.clientes.get(Cli).println(msg);
	     System.out.println(msg);
	   }
	   

	   
	      
	 }
