import java.io.File;
import java.io.InputStream;
import java.util.Scanner;

 public class TrataCliente implements Runnable {
 
   private InputStream cliente;
   private Servidor servidor;
   private int numCli;
   private String ipCliente;
   private int portCli;

 
   public TrataCliente(InputStream cliente, Servidor servidor,int numCli, String ipCliente, int portCli) {
     this.cliente = cliente;
     this.servidor = servidor;
     this.numCli = numCli;
     this.ipCliente = ipCliente;
     this.portCli = portCli;
   }
 
   public void run() {
     // quando chegar uma msg, identifica o arquivo desejado e o divide em n partes e envia
     
	 Scanner s = new Scanner(this.cliente);
     //Separa os argumentos do cliente - nome do arquivo;numero de partes para enviar
     String[] args = s.nextLine().split(";");
     
     if (args.length > 2)
    	 System.out.println("Tamanho incorreto de parametros.");
     else
    	 if(args.length == 2) {
    		 //Detecta arquivo
    		 File file = new File(args[0]);
    		 int parts =Integer.parseInt(args[1]);
    		 
    		 if (parts > 1){
    			 FileSplitter splitter = new FileSplitter(file);
    			 System.out.println("Divisao ok: " + splitter.split(parts)); 
    		 
	    		 if(Integer.parseInt(args[1]) > 1){
	    			 servidor.enviaMensagem("Divisão dos arquivos realizada",numCli);
	    		 }
    		 }
    		 servidor.enviaMensagem("Preparando para enviar arquivo[s]",numCli);
    		 
    		 //Avisa que vai enviar os parametros
    		 servidor.enviaMensagem("###",numCli);
    		 
    		 //Envia os parametros da tranferencia
    		 servidor.enviaMensagem(file + ":" + this.portCli + ":" + parts + ":" + this.servidor.addrClientes.get(numCli) ,numCli);
    		 
    		 if (parts >1){
	    		 // Cria uma thread pra cada arquivo a ser enviado
	    		 for (int i = 0; i < parts;i++) {
			       
			       EnviaArquivo ea = new EnviaArquivo(file + ".part." + i, this.portCli + (i+1), i);
			       new Thread(ea).start();
			       
			       //System.out.println("Enviando parte " + (i + 1));
	    		 }
    		 }
    		 else{
    			 EnviaArquivo ea = new EnviaArquivo(file.getName(), this.portCli + 1, 0);
			     new Thread(ea).start();
    			 
    		 }
    		 
    		//Avisa que vai pode iniciar a transferencia
    		 servidor.enviaMensagem("###",numCli);
    		
    	 }

     
     
     s.close();
     
   }
 }